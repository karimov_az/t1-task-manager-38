package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.PropertyService;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthEndpointTest {

    @Nullable
    private static String ADMIN_TOKEN;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Test
    public void testLogin() throws Exception {
        assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(null, null))
        );
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        ADMIN_TOKEN = userLoginResponse.getToken();
        assertNotNull(ADMIN_TOKEN);
    }

    @Test
    public void testLogout() throws Exception {
        assertNotNull(ADMIN_TOKEN);
        authEndpoint.logout(new UserLogoutRequest(ADMIN_TOKEN));
        assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest(ADMIN_TOKEN)));
    }

    @Test
    public void testProfile() throws Exception {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        @Nullable final String token = userLoginResponse.getToken();
        assertNotNull(token);

        @NotNull final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(token));
        @Nullable final User user = userProfileResponse.getUser();
        assertNotNull(user);
        assertEquals("test", user.getLogin());

        authEndpoint.logout(new UserLogoutRequest(token));
        assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest(token)));
    }

}
