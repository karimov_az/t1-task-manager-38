package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.model.Project;

import java.sql.ResultSet;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    Project fetch(@NotNull ResultSet resultSet) throws Exception;

    Project update(@NotNull Project project) throws Exception;

}
