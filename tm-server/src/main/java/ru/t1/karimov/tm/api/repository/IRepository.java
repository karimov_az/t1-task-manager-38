package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    int getSize() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    M removeOne(@NotNull M model) throws Exception;

    @Nullable
    M removeOneById(@NotNull String id) throws Exception;

    @Nullable
    M removeOneByIndex(@NotNull Integer index) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

}
