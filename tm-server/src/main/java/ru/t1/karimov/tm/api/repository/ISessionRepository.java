package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.model.Session;

import java.sql.ResultSet;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @NotNull
    Session fetch(@NotNull ResultSet resultSet) throws Exception;

    @NotNull
    Session update(@NotNull Session session) throws Exception;

}
