package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.User;

import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User fetch(@NotNull ResultSet resultSet) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    boolean isLoginExist(@NotNull String login) throws Exception;

    boolean isEmailExist(@NotNull String email) throws Exception;

    void update(@NotNull User model) throws Exception;

}
