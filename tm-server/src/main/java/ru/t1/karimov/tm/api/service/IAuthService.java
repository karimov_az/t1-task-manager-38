package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(@Nullable String login, @Nullable String password) throws Exception;

    void invalidate(@Nullable Session session) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token);

}
