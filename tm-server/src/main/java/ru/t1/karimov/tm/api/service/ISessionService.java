package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
