package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws AbstractFieldException, Exception;

    @NotNull
    List<M> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id) throws AbstractFieldException, Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    M removeOne(@Nullable String userId, @Nullable M model) throws AbstractFieldException, Exception;

    void removeAll(@Nullable String userId) throws AbstractFieldException, Exception;

}
