package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException, Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws AbstractException, Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeOne(@Nullable User model) throws Exception;

    @Nullable
    User removeOneByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeOneByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
