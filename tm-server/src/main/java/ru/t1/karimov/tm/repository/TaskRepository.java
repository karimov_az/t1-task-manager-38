package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_TASK = "tm_task";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    private static final String COLUMN_DESCRIPTION = "description";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    @NotNull
    private static final String COLUMN_PROJECT_ID = "project_id";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_TASK;
    }

    @NotNull
    @Override
    public Task add(@NotNull Task model) throws Exception {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?);",
                getTableName(),
                COLUMN_ID,
                COLUMN_CREATED,
                COLUMN_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_STATUS,
                COLUMN_USER_ID,
                COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().name());
            statement.setString(6, model.getUserId());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public Task add(@NotNull String userId, @NotNull Task model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description
    ) throws Exception {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task fetch(@NotNull ResultSet resultSet) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString(COLUMN_ID));
        task.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        task.setName(resultSet.getString(COLUMN_NAME));
        task.setDescription(resultSet.getString(COLUMN_DESCRIPTION));
        task.setUserId(resultSet.getString(COLUMN_USER_ID));
        task.setStatus(Status.toStatus(resultSet.getString(COLUMN_STATUS)));
        task.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        task.setProjectId(resultSet.getString(COLUMN_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), COLUMN_USER_ID, COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public Task update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_STATUS, COLUMN_PROJECT_ID, COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
