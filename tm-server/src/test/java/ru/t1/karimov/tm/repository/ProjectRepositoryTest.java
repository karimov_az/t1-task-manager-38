package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static List<Project> projectList;

    @NotNull
    private static IProjectRepository projectRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final User user1 = new User();
        user1.setLogin("test1");
        user1.setPasswordHash("test1");
        userRepository.add(user1);
        connection.commit();
        USER1_ID = user1.getId();

        @NotNull final User user2 = new User();
        user2.setLogin("test2");
        user2.setPasswordHash("test2");
        userRepository.add(user2);
        connection.commit();
        USER2_ID = user2.getId();
    }

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository(connection);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(USER1_ID);
            else project.setUserId(USER2_ID);
            projectList.add(project);
            projectRepository.add(project);
            connection.commit();
        }
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        userRepository.removeOneById(USER1_ID);
        connection.commit();
        userRepository.removeOneById(USER2_ID);
        connection.commit();
        connection.close();
    }

    @After
    public void initClear() throws Exception {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            connection.commit();
        }
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Project project = new Project();
        @NotNull final String userId = USER1_ID;
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = project.getId();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(USER1_ID, project);
        connection.commit();
        @Nullable final Project actualProject = projectRepository.findOneById(id);
        assertNotNull(actualProject);
        assertEquals(userId, actualProject.getUserId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());

        projectRepository.removeOne(actualProject);
        connection.commit();
    }

    @Test
    public void testAddAll() throws Exception {
        final int expectedSize = projectRepository.getSize();
        @NotNull List<Project> projects = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String userId = i < NUMBER_OF_ENTRIES / 2 ? USER2_ID : USER1_ID;
            final int num = i + 1 + NUMBER_OF_ENTRIES;
            @NotNull final String name = "Test Project Name " + num;
            @NotNull final String description = "Test Project Description " + num;
            @NotNull final Project project = new Project(userId, name, description, Status.IN_PROGRESS);
            projects.add(project);
        }
        projectRepository.add(projects);
        connection.commit();
        final int actualSize = projectRepository.getSize();
        assertEquals(expectedSize + NUMBER_OF_ENTRIES, actualSize);

        for (@NotNull final Project project : projects) {
            projectRepository.removeOne(project);
            connection.commit();
        }
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAll(USER1_ID);
        connection.commit();
        assertEquals(emptyList, projectRepository.findAll(USER1_ID));
        assertNotEquals(emptyList, projectRepository.findAll(USER2_ID));
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        projectRepository.removeAll("Other_id");
        connection.commit();
        final int numberAllProjects = projectRepository.getSize(USER1_ID) + projectRepository.getSize(USER2_ID);
        assertEquals(NUMBER_OF_ENTRIES, numberAllProjects);
    }

    @Test
    public void testFindAllByUser() throws Exception {
        @NotNull final List<Project> expectedProjects = projectList.stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER1_ID);
        assertEquals(expectedProjects.size(), actualProjectList.size());
    }

    @Test
    public void testFindAllSortByUser() throws Exception {
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER1_ID, sort.getComparator());
            @NotNull final List<Project> expectedProjectList = projectList.stream()
                    .filter(m -> USER1_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final Project actualProject = actualProjectList.get(i);
                @NotNull final Project expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER2_ID, sort.getComparator());
            @NotNull final List<Project> expectedProjectList = projectList.stream()
                    .filter(m -> USER2_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final Project actualProject = actualProjectList.get(i);
                @NotNull final Project expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }
    }

    @Test
    public void testFindOneByIdPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final Project actualProject = projectRepository.findOneById(id);
            assertNotNull(actualProject);
            assertEquals(id, actualProject.getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = UUID.randomUUID().toString();
            assertNull(projectRepository.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            @Nullable final Project actualProject = projectRepository.findOneById(userId, id);
            assertNotNull(actualProject);
            assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(projectRepository.findOneById(USER1_ID, UUID.randomUUID().toString()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() throws Exception {
        int i = 0;
        for (@NotNull final Project project : projectList) {
            @Nullable final Project actualProject = projectRepository.findOneByIndex(i);
            assertNotNull(actualProject);
            assertNotNull(project);
            i++;
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws Exception {
        final int expectedSize = projectRepository.getSize();
        assertNull(projectRepository.findOneByIndex(expectedSize + NUMBER_OF_ENTRIES));
    }

    @Test
    public void testFindOneByIndexForUserPositive() throws Exception {
        int i = 0, j = 0;
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (USER1_ID.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.findOneByIndex(USER1_ID, i);
                assertNotNull(actualProject);
                assertEquals(project.getId(), actualProject.getId());
                i++;
            }
            if (USER2_ID.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.findOneByIndex(USER2_ID, j);
                assertNotNull(actualProject);
                assertEquals(project.getId(), actualProject.getId());
                j++;
            }
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() throws Exception {
        final int expectedSize = projectRepository.getSize();
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(projectRepository.findOneByIndex(userId, expectedSize + NUMBER_OF_ENTRIES));
    }

    @Test
    public void testExistByIdPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(id));
        }
    }

    @Test
    public void testExistByIdNegative() throws Exception {
        @NotNull final String id = UUID.randomUUID().toString();
        assertFalse(projectRepository.existsById(id));
    }

    @Test
    public void testExistByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        final int expectedSize = NUMBER_OF_ENTRIES / userList.size();
        for (@NotNull final String userId : userList) {
            final int projectRepositorySize = projectRepository.getSize(userId);
            @Nullable final List<Project> projectsUser = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(projectsUser.size(), projectRepositorySize);
            assertEquals(expectedSize, projectRepositorySize);
        }
    }

    @Test
    public void testRemovePositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            connection.commit();
            assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() throws Exception {
        final int expectedSize = projectRepository.getSize();
        @NotNull final Project project = new Project();
        projectRepository.removeOne(project);
        connection.commit();
        final int actualSize = projectRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        final int expectedSize = projectRepository.getSize();
        for (@NotNull final Project project : projectList) {
            assertNotNull(projectRepository.removeOneById(project.getId()));
            connection.commit();
            assertNull(projectRepository.findOneById(project.getId()));
        }
        final int actualSize = projectRepository.getSize();
        assertNotEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        final int expectedSize = projectRepository.getSize();
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        assertNull(projectRepository.removeOneById(idNotFromRepository));
        connection.commit();
        final int actualSize = projectRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            assertNotNull(userId);
            assertNotNull(projectRepository.removeOneById(userId, id));
            connection.commit();
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        assertNull(projectRepository.removeOneById(USER1_ID, otherId));
        connection.commit();
        assertNull(projectRepository.removeOneById(USER2_ID, otherId));
        connection.commit();
        assertNull(projectRepository.removeOneById(otherUserId,otherId));
        connection.commit();
    }

    @Test
    public void testRemoveByIndexForUserPositive() throws Exception {
        final int expectedSize = projectRepository.getSize();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = projectList.get(i);
            @Nullable final String userId = project.getUserId();
            if (USER1_ID.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.removeOneByIndex(USER1_ID, 0);
                connection.commit();
                assertNotNull(actualProject);
                assertEquals(project.getId(), actualProject.getId());
            }
            if (USER2_ID.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.removeOneByIndex(USER2_ID, 0);
                connection.commit();
                assertNotNull(actualProject);
                assertEquals(project.getId(), actualProject.getId());
            }
        }
        final int actualSize = projectRepository.getSize();
        assertEquals(expectedSize - NUMBER_OF_ENTRIES, actualSize);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexForUserNegative() throws Exception {
        final int expectedSize = projectRepository.getSize();
        final int index = expectedSize + NUMBER_OF_ENTRIES;
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        assertNull(projectRepository.removeOneByIndex(otherUserId, index));
    }

}
