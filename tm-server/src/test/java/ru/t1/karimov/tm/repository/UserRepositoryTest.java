package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static List<User> userList;

    @NotNull
    private static IUserRepository userRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @Before
    public void initRepository() throws Exception {
        userList = new ArrayList<>();
        userRepository = new UserRepository(connection);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setPasswordHash("user" + i);
            user.setEmail("user" + i + "@tst.ru");
            if (i <= 7) user.setRole(Role.USUAL);
            else user.setRole(Role.ADMIN);
            userRepository.add(user);
            connection.commit();
            userList.add(user);
        }
    }

    @After
    public void initClear() throws Exception {
        for (@NotNull final User user : userList) {
            userRepository.removeOne(user);
            connection.commit();
        }
        userList.clear();
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        connection.commit();
        connection.close();
    }

    @Test
    public void testAdd() throws Exception {
        final int expectedSize = userRepository.getSize();
        @NotNull final User user = new User();
        user.setLogin("test100");
        user.setPasswordHash("test100");
        user.setEmail("test100@tst.ru");
        userRepository.add(user);
        assertEquals(expectedSize + 1, userRepository.getSize());

        userRepository.removeOne(user);
        connection.commit();
    }

    @Test
    public void testAddAll() throws Exception {
        final int startedSize = userRepository.getSize();
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("test" + i);
            user.setPasswordHash("test" + i);
            user.setEmail("test" + i + "@tst.ru");
            users.add(user);
        }
        userRepository.add(users);
        connection.commit();
        final int expectedSize = startedSize + NUMBER_OF_ENTRIES;
        assertEquals(expectedSize, userRepository.getSize());

        for (@NotNull final User user : users) {
            userRepository.removeOne(user);
            connection.commit();
        }
    }

    @Test
    public void testFindAll() throws Exception {
        final int expectedSize = userRepository.getSize();
        @NotNull final List<User> actualUserList = userRepository.findAll();
        assertEquals(expectedSize, actualUserList.size());
    }

    @Test
    public void testFindByIdPositive() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final User actualUser = userRepository.findOneById(id);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
        }
    }

    @Test
    public void testFindByIdNegative() throws Exception {
        @NotNull final String id = UUID.randomUUID().toString();
        assertNull(userRepository.findOneById(id));
    }

    @Test
    public void testFindByLoginPositive() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            @Nullable final User actualUser = userRepository.findByLogin(login);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
            assertEquals(login, actualUser.getLogin());
        }
    }

    @Test
    public void testFindByLoginNegative() throws Exception {
        @NotNull final String login = "OtherLogin";
        assertNull(userRepository.findByLogin(login));
    }

    @Test
    public void testFindByEmailPositive() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            @Nullable final User actualUser = userRepository.findByEmail(email);
            assertNotNull(actualUser);
            assertEquals(user.getId(), actualUser.getId());
            assertEquals(email, actualUser.getEmail());
        }
    }

    @Test
    public void testFindByEmailNegative() throws Exception {
        @NotNull final String email = "OtherEmail";
        assertNull(userRepository.findByEmail(email));
    }

    @Test
    public void testIsLoginExistPositive() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            assertTrue(userRepository.isLoginExist(login));
        }
    }

    @Test
    public void testIsLoginExistNegative() throws Exception {
        @Nullable final String login = "OtherLogin";
        assertFalse(userRepository.isLoginExist(login));
    }

    @Test
    public void testisEmailExistPositive() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            assertTrue(userRepository.isEmailExist(email));
        }
    }

    @Test
    public void testEmailExistNegative() throws Exception {
        @Nullable final String email = "OtherEmail";
        assertFalse(userRepository.isEmailExist(email));
    }

    @Test
    public void testRemovePositive() throws Exception {
        final int startedSize = userRepository.getSize();
        for (@NotNull final User user : userList) {
            userRepository.removeOne(user);
            connection.commit();
            assertNull(userRepository.findOneById(user.getId()));
        }
        final int actualSize = userRepository.getSize();
        final int expectedSize = startedSize - userList.size();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveNegative() throws Exception {
        final int expectedSize = userRepository.getSize();
        @NotNull final User user = new User();
        userRepository.removeOne(user);
        connection.commit();
        final int actualSize = userRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        final int startedSize = userRepository.getSize();
        for (@NotNull final User user : userList) {
            @Nullable final String userId = user.getId();
            assertNotNull(userRepository.removeOneById(userId));
            connection.commit();
            assertNull(userRepository.findOneById(userId));
        }
        final int actualSize = userRepository.getSize();
        final int expectedSize = startedSize - userList.size();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        final int expectedSize = userRepository.getSize();
        @NotNull final String otherId = UUID.randomUUID().toString();
        assertNull(userRepository.removeOneById(otherId));
        connection.commit();
        final int actualSize = userRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

}
